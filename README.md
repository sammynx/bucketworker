# bucketworker

Write Raven events to a bucket.

<hr>

Worker consumes <max_intake> messages and creates one blob of it to store in the configured bucket.
Message metadata is not stored, only content.

Config Options:

- WithMaxIntake(string) Set the max amount of messages to put in blob. ex.: "1000"

- WithCompress(string) Set compression algo to use on blob. Options: gzip, zlib, none

- WithBucket(string) Set the bucket url. The scheme tells which cloudservice to use and the hostname is the bucketname [Required]. ex. "s3://my-bucket"

- WithObjectName(string) - Set the name for the object to store in the bucket.
