// Package main provides bucketworker which reads a burst of messages and stores the messages content in a bucket.
package main

import (
	"os"

	rw "github.com/dutchsec/raven-worker"

	_ "gocloud.dev/blob/azureblob"
	_ "gocloud.dev/blob/gcsblob"
	_ "gocloud.dev/blob/s3blob"
)

var log = rw.DefaultLogger

func main() {

	options := []OptionFn{
		WithMaxIntake(os.Getenv("W_MAX_INTAKE")),
		WithCompress(os.Getenv("W_COMPRESS")),
		WithBucket(os.Getenv("W_BUCKET_URL")),
		WithObjectName(os.Getenv("W_OBJECT_NAME")),
		WithGrantee(os.Getenv("W_GRANTEE_READ")),
	}

	// Defaults:
	opts := &Options{
		MaxIntake:   1000,
		CompressAlg: "none",
	}

	for _, fn := range options {
		fn(opts)
	}

	worker, err := rw.New(rw.DefaultEnvironment())
	if err != nil {
		log.Fatalf("creating worker: %v", err)
	}

	cache, err := MessageBuffer(opts)
	if err != nil {
		log.Fatalf("creating message cache: %v", err)
	}

	intake := 0
	for ; intake < opts.MaxIntake; intake++ {

		ref, err := worker.Consume()
		if err != nil {
			// no more messages, stop intake.
			break
		}

		m, err := worker.Get(ref)
		if err != nil {
			log.Errorf("get: %v", err)
			continue
		}

		// store the event in the cache.
		cache.Put(m)

		// message can be acked and dumped now.
		err = worker.Ack(ref, rw.WithFilter())
		if err != nil {
			log.Fatalf("ack: %v", err)
		}
	}

	log.Debugf("Got %d messages", intake-1)

	// persist collected data in the cloud.
	if err := cache.Close(); err != nil {
		log.Fatalf("toBucket: %v", err)
	}
}
