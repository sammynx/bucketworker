package main

import (
	"strconv"
	"strings"
)

type Options struct {
	MaxIntake         int
	CompressAlg       string
	Bucket            string
	ObjectName        string
	AWSRegion         string
	S3AccessKey       string
	S3SecretAccessKey string
	Grantee           string
}

type OptionFn func(*Options)

func WithGrantee(s string) OptionFn {
	return func(o *Options) {
		o.Grantee = s
	}
}

func WithAWSRegion(region string) OptionFn {
	return func(o *Options) {
		o.AWSRegion = region
	}
}

func WithAWSSecretAccessKey(key string) OptionFn {
	return func(o *Options) {
		o.S3SecretAccessKey = key
	}
}

func WithAWSAccessKey(key string) OptionFn {
	return func(o *Options) {
		o.S3AccessKey = key
	}
}

func WithBucket(u string) OptionFn {
	return func(o *Options) {
		if u == "" {
			log.Fatalf("(W_BUCKET_URL) required bucket url is missing!")
		}
		o.Bucket = u
	}
}

func WithObjectName(name string) OptionFn {
	return func(o *Options) {
		if name == "" {
			log.Fatalf("(W_OBJECT_NAME) required objectname missing")
		}
		o.ObjectName = name
	}
}

func WithMaxIntake(num string) OptionFn {
	return func(o *Options) {
		if num != "" {
			n, err := strconv.Atoi(num)
			if err != nil {
				log.Fatalf("(W_MAX_INTAKE) %v", err)
			}
			o.MaxIntake = n
		}
	}
}

func WithCompress(alg string) OptionFn {
	return func(o *Options) {
		if alg == "" {
			return
		}

		a := strings.ToLower(alg)
		if !contains(a, "gzip", "zlib", "none") {
			log.Fatalf("(W_COMPRESS) unsupported compression method: %s", a)
		}
		o.CompressAlg = a
	}
}

// check if s is in items
func contains(s string, items ...string) bool {
	for _, it := range items {
		if s == it {
			return true
		}
	}
	return false
}
