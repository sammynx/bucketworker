FROM golang:1.13 AS builder

ENV GOPATH /go
# ENV GO111MODULE=on

COPY . /go/src/go.dutchsec.com/worker/

WORKDIR /go/src/go.dutchsec.com/worker

RUN go build -o /app main.go options.go cache.go
    
FROM bitnami/minideb-extras:jessie
COPY --from=builder /go/src/go.dutchsec.com/worker/ingestion-spec.template /
COPY --from=builder /app /app
    
ENTRYPOINT ["/app"]
