module bucketworker

go 1.13

require (
	github.com/aws/aws-sdk-go v1.19.45
	github.com/dutchsec/raven-worker v0.2.2
	gocloud.dev v0.17.0
)
