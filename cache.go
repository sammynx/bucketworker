package main

import (
	"compress/gzip"
	"compress/zlib"
	"context"
	"fmt"
	"io"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	rw "github.com/dutchsec/raven-worker"
	"gocloud.dev/blob"
	_ "gocloud.dev/blob/azureblob"
	_ "gocloud.dev/blob/gcsblob"
	_ "gocloud.dev/blob/s3blob"
)

type messageBuffer struct {
	buf        io.Writer    // buffer message content.
	blobWriter *blob.Writer // needed for closing.
}

func MessageBuffer(opts *Options) (*messageBuffer, error) {

	// Open a connection to the bucket.
	b, err := blob.OpenBucket(context.Background(), opts.Bucket)
	if err != nil {
		return nil, fmt.Errorf("Failed to setup bucket: %s", err)
	}

	beforeWrite := func(as func(interface{}) bool) error {
		var uploadOpts *s3manager.UploadInput
		if as(&uploadOpts) {
			uploadOpts.GrantRead = aws.String(opts.Grantee)
		}
		return nil
	}

	writeOpts := &blob.WriterOptions{BeforeWrite: beforeWrite}

	buf, err := b.NewWriter(context.Background(), opts.ObjectName, writeOpts)
	if err != nil {
		return nil, fmt.Errorf("Failed to setup bucket: %s", err)
	}

	cache := &messageBuffer{
		blobWriter: buf,
	}

	switch opts.CompressAlg {
	case "gzip":
		cache.buf = gzip.NewWriter(buf)
	case "zlib":
		cache.buf = zlib.NewWriter(buf)
	default:
		cache.buf = buf
	}

	return cache, nil
}

func (c *messageBuffer) Put(m rw.Message) {

	_, err := c.buf.Write([]byte(m.Content))
	if err != nil {
		log.Debugf("Put Write error: %v. Closing...", err)
		err = c.Close()
		panic(err)
	}
}

//Close will close the compression writers if any and commit the write to the bucket provider.
func (c *messageBuffer) Close() error {
	var err error

	if b, ok := c.buf.(*gzip.Writer); ok {
		err = b.Close()
	}

	if b, ok := c.buf.(*zlib.Writer); ok {
		err = b.Close()
	}
	if err != nil {
		return err
	}
	return c.blobWriter.Close()
}
